public class Color {
    // ANSI escape codes.
    public static String YELLOW = "\u001B[33m";
    public static String RED = "\u001B[31m";
    public static String CYAN = "\u001B[36m";
    public static String RESET = "\u001B[0m";

    public static void clearScreen() {
        System.out.println(new String(new char[50]).replace("\0", "\r\n"));
    }
}
