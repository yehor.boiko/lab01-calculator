import java.util.InputMismatchException;

public class Calculator {
    protected static int digitsAfterComma = 4;

    protected static double summation(double val1, double val2) {
        return Calculator.roundValue(val1 + val2);
    }
    protected static double substraction(double val1, double val2) {
        return Calculator.roundValue(val1 - val2);
    }
    protected static double multiplication(double val1, double val2) {
        return Calculator.roundValue(val1 * val2);
    }
    protected static double division(double val1, double val2) {
        if(val2 == 0)
            throw new ArithmeticException(Color.RED + "Дiлення на нуль не можливе!" + Color.RESET);

        return Calculator.roundValue(val1 / val2);
    }

    protected static double roundValue(double val) {
        double divisor = Math.pow(10, Calculator.digitsAfterComma);
        // Round the value to the specified number of digits after the decimal point.
        return Math.round(val * divisor) / divisor;
    }

    protected static void displayCalculatorContextMenu() {
        System.out.println(Color.YELLOW + "Оберiть операцiю:" + Color.CYAN);
        System.out.println("1. Додавання");
        System.out.println("2. Вiднiмання");
        System.out.println("3. Множення");
        System.out.println("4. Дiлення");
        System.out.println("5. Вийти з калькулятора");
        System.out.print(Color.RESET);
    }

    public static void run() {
        // Initialize common variables.
        int option;
        double val1 = 0.0, val2 = 0.0, result = 0;

        try {
            // Ask for the first number.
            System.out.print("Введiть перше число: ");
            val1 = Main.in.nextDouble();
        }
        catch (java.util.InputMismatchException e) {
            throw new NumberFormatException(Color.RED + "В це поле можна ввести лише числа!" + Color.RESET);
        }

        // Run calculator.
        while(true) {
            // Get the operation code.
            Calculator.displayCalculatorContextMenu();
            option = Main.in.nextInt();
            // Exit the calculator.
            if(option == 5)
                break;
                // Check if the operation exists.
            else if(option < 1 || option > 4)
                throw new IllegalArgumentException(Color.RED + "Такої операцiї не iснує!" + Color.RESET);

            try {
                // Get second number and operation code.
                System.out.print("Введiть друге число: ");
                val2 = Main.in.nextDouble();
            }
            catch (java.util.InputMismatchException e) {
                throw new NumberFormatException(Color.RED + "В це поле можна ввести лише числа!" + Color.RESET);
            }

            // Execute calculator operation.
            switch(option) {
                case 1:
                    result = Calculator.summation(val1, val2);
                    break;

                case 2:
                    result = Calculator.substraction(val1, val2);
                    break;

                case 3:
                    result = Calculator.multiplication(val1, val2);
                    break;

                case 4:
                    result = Calculator.division(val1, val2);
                    break;
            }

            // Assign result to the first number.
            val1 = result;

            Color.clearScreen();
            System.out.println(Color.YELLOW + ("Поточний результат: " + result) + Color.RESET);
        }
    }
}