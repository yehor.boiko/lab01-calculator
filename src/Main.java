import java.util.Scanner;

public class Main {
    protected static Scanner in;

    public static void main(String[] args) {
        // Initialize scanner for I/O operations and common variables.
        Main.in = new Scanner(System.in);
        int option;

        // Run kernel. 
        while(true) {
            Color.clearScreen();
            Main.displayMainContextMenu();
            option = Main.in.nextInt();
            // Exit the calculator.
            if(option == 5)
                break;
                // Check if the operation exists.
            else if(option < 1 || option > 5)
                throw new IllegalArgumentException(Color.RED + "Такого застосунку не має!" + Color.RESET);

            if(option == 1) {
                Calculator.run();
            } else if(option == 2) {
                AreaConverter.run();
            } else if(option == 3) {
                VolumeConverter.run(in);
            } else if(option == 4) {
                TimeConverter.showTimeConverter();
            }
        }

        Main.in.close();
    }

    public static void displayMainContextMenu() {
        System.out.println(Color.YELLOW + "Оберіть застосунок:" + Color.CYAN);
        System.out.println("1. Калькулятор");
        System.out.println("2. Перетворення площин");
        System.out.println("3. Перетворення об'єму");
        System.out.println("4. Перетворення часу");
        System.out.println("5. Вийти з програми");
        System.out.print(Color.RESET);
    }
}