import java.util.Scanner;

public class VolumeConverter {
    static final private int CHOICE_EXIT_CODE = 0;
    static final private int MEASURE_LITER_CODE = 1;
    static final private int MEASURE_MILLILITER_CODE = 2;
    static final private int MEASURE_CUBIC_METER_CODE = 3;

    public static void run(Scanner scanner) {
        String measureChoise = "Виберiть мiру об'єму. \n1. Лiтр \n2. Мiлiлiтр \n3. Метр^3 \nВведiть 1, 2, або 3(щоб вийти введiть 0): ";
        int initialMeasure, finalMeasure;
        double volume, result;

        while (true) {
            System.out.print(measureChoise);

            initialMeasure = scanner.nextInt();

            if (initialMeasure == CHOICE_EXIT_CODE) {
                System.out.println();
                break;
            }

            if (initialMeasure > 3) {
                throw new IllegalArgumentException("Такого коду не iснує.");
            }

            System.out.println();

            System.out.print(measureChoise);

            finalMeasure = scanner.nextInt();

            if (finalMeasure == CHOICE_EXIT_CODE) {
                System.out.println();
                break;
            }

            if (finalMeasure > 3) {
                throw new IllegalArgumentException("Такого коду не iснує.");
            }

            System.out.println();

            System.out.print("Введiть об'єм: ");

            volume = scanner.nextDouble();

            try {
                result = convertVolume(volume, initialMeasure, finalMeasure);
                System.out.println("Результат: " + result);
            } catch (Exception e) {
                System.out.println("Помилка: " + e.getMessage());
            }

            System.out.println();
        }
    }

    private static double convertVolume(double volume, final int initialMeasure, final int finalMeasure) {
        switch (initialMeasure) {
            case MEASURE_LITER_CODE:
                if (finalMeasure == MEASURE_MILLILITER_CODE) {
                    volume *= 1000;
                } else if (finalMeasure == MEASURE_CUBIC_METER_CODE) {
                    volume /= 1000;
                }
                break;
            case MEASURE_MILLILITER_CODE:
                if (finalMeasure == MEASURE_LITER_CODE) {
                    volume /= 1000;
                } else if (finalMeasure == MEASURE_CUBIC_METER_CODE) {
                    volume /= 1000000;
                }
                break;
            case MEASURE_CUBIC_METER_CODE:
                if (finalMeasure == MEASURE_LITER_CODE) {
                    volume *= 1000;
                } else if (finalMeasure == MEASURE_MILLILITER_CODE) {
                    volume *= 1000000;
                }
                break;
            default:
                break;
        }

        return volume;
    }
}
