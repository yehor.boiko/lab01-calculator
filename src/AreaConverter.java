public class AreaConverter {
    protected static int digitsAfterComma = 10;

    protected static double squareMetersToSquareCentimeters(double squareMeters) {
        return AreaConverter.roundValue(squareMeters * 10000.0);
    }

    protected static double squareMetersToSquareMillimeters(double squareMeters) {
        return AreaConverter.roundValue(squareMeters * 1_000_000.0);
    }

    protected static double squareCentimetersToSquareMeters(double squareCentimeters) {
        return AreaConverter.roundValue(squareCentimeters / 10000.0);
    }

    protected static double squareCentimetersToSquareMillimeters(double squareCentimeters) {
        return AreaConverter.roundValue(squareCentimeters * 100.0);
    }

    protected static double squareMillimetersToSquareMeters(double squareMillimeters) {
        return AreaConverter.roundValue(squareMillimeters / 1_000_000.0);
    }

    protected static double squareMillimetersToSquareCentimeters(double squareMillimeters) {
        return AreaConverter.roundValue(squareMillimeters / 100.0);
    }

    protected static double roundValue(double val) {
        double divisor = Math.pow(10, AreaConverter.digitsAfterComma);
        // Round the value to the specified number of digits after the decimal point.
        return Math.round(val * divisor) / divisor;
    }

    protected static void displayAreaConverterContextMenu() {
        System.out.println(Color.YELLOW + "Оберiть операцiю:" + Color.CYAN);
        System.out.println("1. Метр^2 до Сантиметра^2");
        System.out.println("2. Метр^2 до Мiлiметра^2");
        System.out.println("3. Сантиметр^2 до Метра^2");
        System.out.println("4. Сантиметр^2 до Мiлiметра^2");
        System.out.println("5. Мiлiметр^2 до Метра^2");
        System.out.println("6. Мiлiметр^2 до Сантиметр^2");
        System.out.println("7. Вийти з калькулятора площини");
        System.out.print(Color.RESET);
    }

    public static void run() {
        // Initialize common variables.
        int option;
        double val, result = 0;

        // Run area converter.
        while(true) {
            // Get the operation code.
            AreaConverter.displayAreaConverterContextMenu();
            option = Main.in.nextInt();
            // Exit the calculator.
            if(option == 7)
                break;
                // Check if the operation exists.
            else if(option < 1 || option > 7)
                throw new IllegalArgumentException(Color.RED + "Такої операцiї не iснує!" + Color.RESET);

            try {
                // Ask for number.
                System.out.print("Введiть число: ");
                val = Main.in.nextDouble();
            }
            catch (java.util.InputMismatchException e) {
                throw new NumberFormatException(Color.RED + "В це поле можна ввести лише число!" + Color.RESET);
            }

            // Execute area converter operation.
            switch(option) {
                case 1:
                    result = AreaConverter.squareMetersToSquareCentimeters(val);
                    break;

                case 2:
                    result = AreaConverter.squareMetersToSquareMillimeters(val);
                    break;

                case 3:
                    result = AreaConverter.squareCentimetersToSquareMeters(val);
                    break;

                case 4:
                    result = AreaConverter.squareCentimetersToSquareMillimeters(val);
                    break;

                case 5:
                    result = AreaConverter.squareMillimetersToSquareMeters(val);
                    break;

                case 6:
                    result = AreaConverter.squareMillimetersToSquareCentimeters(val);
                    break;
            }

            Color.clearScreen();
            System.out.println(Color.YELLOW + ("Результат: " + result) + Color.RESET);
        }
    }
}
