# Дослідження система контроля версій GIT та налагодження оточення Java/Kotlin розробника

# Мета роботи

Навчитися налагоджувати оточення Java/Kotlin-розробника. Вивчити основи використання системи GIT.

# Результати

- [x] Завдання 0 (обов'язкове)
- [x] Завдання 2 (командне 75-100%)

## Завдання 0 (обов’язкове)

1.  Тренінг з Git

    <p align="center">  
        ![Рис. 1. Тренінг з Git (1)](images/certificate1.png)
        123
    </p>

    <p align="center">
        Рис. 1. <b>Тренінг з Git (1)</b>
    </p>
    
    <p align="center">
        123
    </p>

    <p align="center">
        ![Рис. 2. Тренінг з Git (2)](images/certificate2.png)
    </p>

    <p align="center">
        Рис. 2. <b>Тренінг з Git (2)</b>
    </p>

   [Знизу Ви знайдете зображення з проходження кожного рівня тренінгу](#course-images)

## Завдання 2 (командне 75-100%)

2. Основне завдання

    <p align="center">
        ![Рис. 3](images/1.png)
    </p>

    <p align="center">Рис. 3. <b>Створення репозиторію</b></p> 

    <p align="center">
    ![Рис. 4](images/2.png)</p>
    <p align='center'>Рис. 4. <b>Toptal .gitignore</b></p>

    <p align="center">![Рис. 5](images/3.png)</p>
    <p align='center'>Рис. 5. <b>Initialize repo</b></p>

    <p align="center">![Рис. 6](images/4.png)</p>
    <p align='center'>Рис. 6. <b>Set git config</b></p>

    <p align="center">![Рис. 7](images/5.png)</p>
    <p align='center'>Рис. 7. <b>Створення гілки Color</b></p>

    <p align="center">![Рис. 8](images/6.png)</p>
    <p align='center'>Рис. 8. <b>Перевірка статусу</b></p>

    <p align="center">![Рис. 9](images/7.png)</p>
    <p align='center'>Рис. 9. <b>Commit змін</b></p>

    <p align="center">![Рис. 10](images/8.png)</p>
    <p align='center'>Рис. 10. <b>Злиття гілок</b></p>

    <p align="center">![Рис. 11](images/9.png)</p>
    <p align='center'>Рис. 11. <b>Git Graph</b></p>

    <p align="center">![Рис. 12](images/10.png)</p>
    <p align='center'>Рис. 12. <b>Створення гілки calculator-class</b></p>

    <p align="center">![Рис. 13](images/11.png)</p>
    <p align='center'>Рис. 13. <b>Diff</b></p>

    <p align="center">![Рис. 14](images/12.png)</p>
    <p align='center'>Рис. 14. <b>Злиття гілки calculator-class</b></p>

    <p align="center">![Рис. 15](images/13.png)</p>
    <p align='center'>Рис. 15. <b>Перегляд локальних гілок</b></p>

    <p align="center">![Рис. 16](images/14.png)</p>
    <p align='center'>Рис. 16. <b>Відправлення змін з локальної гілки calculator-class до віддаленого репозиторію</b></p>

    <p align="center">![Рис. 17](images/15.png)</p>
    <p align='center'>Рис. 17. <b>Відправлення змін з локальної гілки main до віддаленого репозиторію</b></p>

    <p align="center">![Рис. 18](images/16.png)</p>
    <p align='center'>Рис. 18. <b>Git Graph</b></p>

    <p align="center">![Рис. 19](images/17.png)</p>
    <p align='center'>Рис. 19. <b>Commit</b></p>

    <p align="center">![Рис. 20](images/18.png)</p>
    <p align='center'>Рис. 20. <b>Злиття гілки main-class</b></p>

    <p align="center">![Рис. 21](images/19.png)</p>
    <p align='center'>Рис. 21. <b>Відправлення зміе з локальної гілки main-class до віддаленого репозиторію</b></p>

    <p align="center">![Рис. 22](images/20.png)</p>
    <p align='center'>Рис. 22. <b>Відправлення зміе з локальної гілки main до віддаленого репозиторію</b></p>

    <p align="center">![Рис. 23](images/21.png)</p>
    <p align='center'>Рис. 23. <b>Git Graph</b></p>

    <p align="center">![Рис. 24](images/22.png)</p>
    <p align='center'>Рис. 24. <b>Фікс до main-class. Злиття main-class. Відправлення на відаленний репозиторій</b></p>

    <p align="center">![Рис. 25](images/23.png)</p>
    <p align='center'>Рис. 25. <b>Git Graph</b></p>

    <p align="center">![Рис. 26](images/24.png)</p>
    <p align='center'>Рис. 26. <b>Внесення змін до color-class</b></p>

    <p align="center">![Рис. 27](images/25.png)</p>
    <p align='center'>Рис. 27. <b>Злиття нової color-class</b></p>

    <p align="center">![Рис. 28](images/26.png)</p>
    <p align='center'>Рис. 28. <b>Git Graph</b></p>

    <p align="center">![Рис. 29](images/27.png)</p>
    <p align='center'>Рис. 29. <b>Main-class branch refactoring</b></p>

    <p align="center">![Рис. 30](images/28.png)</p>
    <p align='center'>Рис. 30. <b>Git Graph</b></p>

    <p align="center">![Рис. 31](images/29.png)</p>
    <p align='center'>Рис. 31. <b>Final Git Graph з індивідуальним завданням</b></p>

    <p align="center">![Рис. 32](images/30.png)</p>
    <p align='center'>Рис. 32. <b>Final Git Graph in Intellij Idea</b></p>

   ## Індивідуальне завдання

    <p align="center">![Рис. 33](images/31.png)</p>
    <p align='center'>Рис. 33. <b>Запрошення члена команди у репозиторій</b></p>

    <p align="center">![Рис. 34](images/32.png)</p>
    <p align='center'>Рис. 34. <b>Reset через певну помилку</b></p>

    <p align="center">![Рис. 35](images/34.png)</p>
    <p align='center'>Рис. 35. <b>Злиття гілки створеної членом команди</b></p>

    <p align="center">![Рис. 36](images/35.png)</p>
    <p align='center'>Рис. 36. <b>Git Graph in Intellij Idea</b></p>

    <p align="center">![Рис. 37](images/36.png)</p>
    <p align='center'>Рис. 37. <b>Run program</b></p>

    <p align="center">![Рис. 38](images/37.png)</p>
    <p align='center'>Рис. 38. <b>Внесення змін до калькулятора</b></p>

    <p align="center">![Рис. 39](images/38.png)</p>
    <p align='center'>Рис. 39. <b>Git Graph (зміни до калькулятора)</b></p>

    <p align="center">![Рис. 40](images/39.png)</p>
    <p align='center'>Рис. 40. <b>Внесення змін до перетворення площин</b></p>

    <p align="center">![Рис. 41](images/40.png)</p>
    <p align='center'>Рис. 41. <b>Злиття змін на локалі. Відправлення main гілки</b></p>

    <p align="center">![Рис. 42](images/41.png)</p>
    <p align='center'>Рис. 42. <b>Git Graph in Intellij Idea</b></p>

   ## Додання мого індивідуального завдання до репозиторію Ростислава

    <p align="center">![Рис. 43](images/42.png)</p>
    <p align='center'>Рис. 43. <b>Створення гілки</b></p>

    <p align="center">![Рис. 44](images/43.png)</p>
    <p align='center'>Рис. 44. <b>Екран Ростислава. Отримання змін інформації з відаленного репозиторію</b></p>

    <p align="center">![Рис. 46](images/44.png)</p>
    <p align='center'>Рис. 46. <b>Екран Ростислава. Злиття створеної гілки з main</b></p>

    <p align="center">![Рис. 47](images/47.png)</p>
    <p align='center'>Рис. 47. <b>Diff</b></p>

    <p align="center">![Рис. 48](images/45.png)</p>
    <p align='center'>Рис. 48. <b>Політки. Заборона внесення змін напряму до main від членів команди</b></p>

    <p align="center">![Рис. 49](images/46.png)</p>
    <p align='center'>Рис. 49. <b>Запрошення члена команди</b></p>

   ## Додання мого індивідуального завдання до репозиторію Артема

    <p align="center">![Рис. 50](images/48.png)</p>
    <p align='center'>Рис. 50. <b>Внесені зміни до репозиторія Артема. Прийняті через Merge Request</b></p>

    <p align="center">![Рис. 51](images/49.png)</p>
    <p align='center'>Рис. 51. <b>Прийняте індивідуальне завдання від Артема через Merge Request</b></p>

   ## Course images
    <p align="center">![Рис. 52](images/course/1.png)</p>
    <p align='center'>Рис. 52. <b>1 завдання</b></p>

    <p align="center">![Рис. 53](images/course/2.png)</p>
    <p align='center'>Рис. 53. <b>2 завдання</b></p>

    <p align="center">![Рис. 54](images/course/3.png)</p>
    <p align='center'>Рис. 54. <b>3 завдання</b></p>

    <p align="center">![Рис. 55](images/course/4.png)</p>
    <p align='center'>Рис. 55. <b>4 завдання</b></p>

    <p align="center">![Рис. 56](images/course/5.png)</p>
    <p align='center'>Рис. 56. <b>5 завдання</b></p>

    <p align="center">![Рис. 57](images/course/6.png)</p>
    <p align='center'>Рис. 57. <b>6 завдання</b></p>

    <p align="center">![Рис. 58](images/course/7.png)</p>
    <p align='center'>Рис. 58. <b>7 завдання</b></p>

    <p align="center">![Рис. 59](images/course/8.png)</p>
    <p align='center'>Рис. 59. <b>8 завдання</b></p>

    <p align="center">![Рис. 60](images/course/9.png)</p>
    <p align='center'>Рис. 60. <b>9 завдання</b></p>

    <p align="center">![Рис. 61](images/course/10.png)</p>
    <p align='center'>Рис. 61. <b>10 завдання</b></p>

    <p align="center">![Рис. 62](images/course/11.png)</p>
    <p align='center'>Рис. 62. <b>11 завдання</b></p>

    <p align="center">![Рис. 63](images/course/12.png)</p>
    <p align='center'>Рис. 63. <b>12 завдання</b></p>

    <p align="center">![Рис. 64](images/course/13.png)</p>
    <p align='center'>Рис. 64. <b>13 завдання</b></p>

    <p align="center">![Рис. 65](images/course/14.png)</p>
    <p align='center'>Рис. 65. <b>14 завдання</b></p>

    <p align="center">![Рис. 66](images/course/15.png)</p>
    <p align='center'>Рис. 66. <b>15 завдання</b></p>

    <p align="center">![Рис. 67](images/course/16.png)</p>
    <p align='center'>Рис. 67. <b>16 завдання</b></p>

    <p align="center">![Рис. 68](images/course/17.png)</p>
    <p align='center'>Рис. 68. <b>17 завдання</b></p>

    <p align="center">![Рис. 69](images/course/18.png)</p>
    <p align='center'>Рис. 69. <b>18 завдання</b></p>

    <p align="center">![Рис. 70](images/course/19.png)</p>
    <p align='center'>Рис. 70. <b>19 завдання</b></p>

    <p align="center">![Рис. 71](images/course/20.png)</p>
    <p align='center'>Рис. 71. <b>20 завдання</b></p>

    <p align="center">![Рис. 72](images/course/21.png)</p>
    <p align='center'>Рис. 72. <b>21 завдання</b></p>

    <p align="center">![Рис. 73](images/course/22.png)</p>
    <p align='center'>Рис. 73. <b>22 завдання</b></p>

    <p align="center">![Рис. 74](images/course/23.png)</p>
    <p align='center'>Рис. 74. <b>23 завдання</b></p>

    <p align="center">![Рис. 75](images/course/24.png)</p>
    <p align='center'>Рис. 75. <b>24 завдання</b></p>

    <p align="center">![Рис. 76](images/course/25.png)</p>
    <p align='center'>Рис. 76 <b>25 завдання</b></p>

    <p align="center">![Рис. 77](images/course/26.png)</p>
    <p align='center'>Рис. 77. <b>26 завдання</b></p>

    <p align="center">![Рис. 78](images/course/27.png)</p>
    <p align='center'>Рис. 78. <b>27 завдання</b></p>

    <p align="center">![Рис. 79](images/course/28.png)</p>
    <p align='center'>Рис. 79. <b>28 завдання</b></p>

    <p align="center">![Рис. 80](images/course/29.png)</p>
    <p align='center'>Рис. 80. <b>29 завдання</b></p>

    <p align="center">![Рис. 81](images/course/30.png)</p>
    <p align='center'>Рис. 81. <b>30 завдання</b></p>

    <p align="center">![Рис. 82](images/course/31.png)</p>
    <p align='center'>Рис. 82. <b>31 завдання</b></p>

### МІНІСТЕРСТВО ОСВІТИ ТА НАУКИ УКРАЇНИ
### ХНЕУ
### ННІ ІНФОРМАЦІЙНИХ ТЕХНОЛОГІЙ

### Introduction Sequence

### Мета роботи

Навчитися налагоджувати оточення Java/Kotlin-розробника. Вивчити основи використання системи GIT.

### Результати

- [x] Завдання 1 (обов'язкове)
- [x] Завдання 2 (командне 75-100%)

### Завдання 1 (обов’язкове)

![Introduction to Git Commits](images/course/1.png)
Introduction to Git Commits

## Дорогим членам команди

Прошу створювати гілки, щоб додати свій функціонал.

```
cd existing_repo
git remote add origin https://gitlab.com/yehor.boiko/lab01-calculator.git
git branch custom
git push origin custom
```
<br>

![Gif. 1](images/giphy.gif)